package io.tecky.repositories;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import io.tecky.models.TransactionRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Scope("singleton")
public class TransactionRecordsRepository {

    private int idCounter = 0;
    private List<TransactionRecord> transactionRecordList;

    private AmazonS3 s3Client;

    @Autowired
    private Environment env;

    private static Logger logger = LoggerFactory.getLogger(TransactionRecordsRepository.class);
    private static int THRESHOLD = 10;

    TransactionRecordsRepository(){

        this.transactionRecordList= new ArrayList<>();
    }

    private void initializeS3Client(){
        Boolean checkS3Client = this.s3Client!=null;
        logger.info("s3Client = "+checkS3Client.toString());
        if(this.s3Client == null){ // lazy initialization
            logger.info("begin to initialize s3Client");
            AWSCredentials awsCredentials = new BasicAWSCredentials(
                    env.getProperty("aws.access.key"),
                    env.getProperty("aws.secret.key")
            );
            var provider = new AWSStaticCredentialsProvider(awsCredentials);
            this.s3Client = AmazonS3ClientBuilder
                    .standard()
                    .withCredentials(provider)
                    .withRegion(Regions.AP_SOUTHEAST_1)
                    .build();
        }
        logger.info("s3Client initialize done");
    }

    public List<TransactionRecord> getTransactionRecords(){
        logger.info("logging TransactionRecords");

        return this.transactionRecordList;
    };

    public int addTransactionRecord(TransactionRecord transactionRecord) {
        logger.info("addData begins here");
        initializeS3Client();
        idCounter++;
        int currentId = idCounter;
        transactionRecord.setId(currentId);
        this.transactionRecordList.add(transactionRecord);

        if(transactionRecordList.size() > THRESHOLD){
            StringBuilder csvBuilder = new StringBuilder();
            // Foreach in Java , similar to for-of loop in javascript
            for(TransactionRecord row : transactionRecordList){
                csvBuilder.append(row.getRegisterDate()).append(",")
                        .append(row.getAddress()).append(",")
                        .append(row.getFloor()).append(",")
                        .append(row.getFlat()).append(",")
                        .append(row.getAreaInFeet()).append(",")
                        .append(row.getDealInMillion()).append(",")
                        .append(row.getPricePerSqrFoot()).append(",")
                        .append(row.getDetail()).append(",")
                        .append(row.getOthers()).append("\n");
            }
            this.s3Client.putObject(env.getProperty("aws.bucket.name"),"record-"+System.currentTimeMillis()+".csv",csvBuilder.toString());
            this.transactionRecordList = new ArrayList<>();
        }

        //        logger.info("Data ["+data+"] is inserted");
        logger.info("addTransactionRecord ends here");
        return transactionRecord.getId();
    }

    public List<TransactionRecord> searchDataByRegisterDate(String registerDate){
        return transactionRecordList.stream().filter(x -> x.getRegisterDate().contains(registerDate)).collect(Collectors.toList());
    }

}
