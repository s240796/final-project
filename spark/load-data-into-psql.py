#%%
import findspark
findspark.init('C:\spark-3.0.1-bin-hadoop3.2')

# please keep your own "db_config" file for replacement.
db_config = {
    "url":'jdbc:postgresql://localhost:5432/daepj',
    "dbtable":'deal_record',
    "user":'s240796',
    "password":"s200804132",
    "driver" :'org.postgresql.Driver'
    }

# # Initialize Spark Session
from pyspark.sql import SparkSession
spark = SparkSession.builder\
    .appName("Deal_Record Datawarehouse").getOrCreate()

bucketname = 'notes.tecky.hk'
folder_name = 'transformed'
folder_extension = 'parquet'

s3_path = ('s3a://{}/transformed/temp/{}*.{}').format(bucketname,folder_name,folder_extension)
df = spark.read.parquet(s3_path)

df = df.withColumn('registerDate', df.registerDate.cast('date'))
df = df.withColumn('address', df.address.cast('string'))
df = df.withColumn('floor', df.floor.cast('string'))
df = df.withColumn('flat', df.flat.cast('string'))
df = df.withColumn('areaInFeet', df.areaInFeet.cast('integer'))
df = df.withColumn('dealInMillion', df.dealInMillion.cast('float'))
df = df.withColumn('pricePerSqrFoot', df.pricePerSqrFoot.cast('integer'))
df = df.withColumn('address1', df.address1.cast('string'))
df = df.withColumn('address2', df.address2.cast('string'))
df = df.withColumn('address7', df.address7.cast('string'))
df = df.withColumn('address8', df.address8.cast('string'))
df = df.withColumn('address9', df.address9.cast('string'))
df = df.withColumn('address10', df.address10.cast('string'))
df = df.withColumn('address11', df.address11.cast('string'))
df = df.filter(df.address!="address")


df.registerTempTable('sodata');

df.write.format('jdbc').options(**db_config).option('dbtable','deal_record').mode('append').save();
df = spark.read.format('jdbc').options(**db_config).option('dbtable','deal_record').load()