import Knex from "knex"

export interface PropDetail{
    id:string;
    estate:string;
    dealInMillion:number;
}

export class PropService{
    public constructor(private knex: Knex){

    }

    public readProp  = async (): Promise<PropDetail[]> => {
        const prop = await this.knex.select('*').from('deal_record')
    
        return prop;
      }
}