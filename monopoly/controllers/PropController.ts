import {PropService} from '../services/PropService'
import express from 'express';

export class PropController{
    public constructor(private propService:PropService){

    }

    public read = async (req:express.Request, res:express.Response)=>{
        res.json(await this.propService.readProp());
    }
}