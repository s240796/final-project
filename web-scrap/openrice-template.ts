import puppeteer from 'puppeteer';
import fs from 'fs';
import dotenv from 'dotenv';
dotenv.config();


const columns=['title','counters','address',
            'price','categories','bookmarks',
            'smile','sad'];

const filePrefix = "openrice_result/restaurants_";

async function main(){
    const browser = await puppeteer.launch({headless:true,defaultViewport:null});
    const page = await browser.newPage();
    let pageNumber = 1;
    let filename = filePrefix+new Date().getTime()+".csv";
    await fs.promises.writeFile(filename,columns.join(',')+"\n",{flag:'a'})
    while(true){
        await page.setUserAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.141 Safari/537.36");
        await page.goto(`https://www.openrice.com/zh/hongkong/restaurants?where=%E4%B8%8A%E7%92%B0&page=${pageNumber}`);
        console.log(`Scrapping Page ${pageNumber}`);
        await page.waitForSelector('.js-poi-list-content-cell-container');
        
        const nextButtonExist = await page.evaluate(()=>{
            return document.querySelector('.pagination-button.js-next') !== null;
        });
        if(!nextButtonExist){
            console.log("End of List");
            break;
        }
        const restaurantTexts = await page.evaluate(()=>{
            const restaurants = document.querySelectorAll('.js-poi-list-content-cell-container .pois-restaurant-list-cell');
            return Array.from(restaurants).map(r=>{
                return {
                    
                    title:(r.querySelector('.title-name') as HTMLElement)?.innerText,
                    counters: (r.querySelector('.counters-container') as HTMLElement)?.innerText,
                    address: (r.querySelector('.address') as HTMLElement)?.innerText,
                    price: (r.querySelector('.icon-info-food-price') as HTMLElement)?.innerText,
                    categories: (r.querySelector('.pois-categoryui-list') as HTMLElement)?.innerText,
                    bookmark:(r.querySelector('.js-bookmark-count') as HTMLElement)?.innerText,
                    dishes:(r.querySelector('.icon-info-signature-dish') as HTMLElement)?.innerText,
                    smile: (r.querySelector('.smile-face')as HTMLElement)?.innerText,
                    sad: (r.querySelector('.sad-face')as HTMLElement)?.innerText
                }
            });
        });
        restaurantTexts.forEach(async (restaurant)=>{
                const row = Object.values(restaurant).join(',');
                await fs.promises.writeFile(filename,row+"\n",{flag:'a'});
        });
        pageNumber++;
        await page.waitForTimeout(2000);
    }

    await browser.close();
  
}


main();