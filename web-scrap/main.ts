import puppeteer from 'puppeteer';
import fs from 'fs';
import dotenv from 'dotenv';
dotenv.config();
import { logger } from './logger';


const columns = ['registerDate', 'address', 'floor', 'room',
    'areaInFeet', 'dealInMillion', 'pricePerSqrFoot'
];


const filePrefix = "./datasets/estate-transaction-list/properties_";

async function loopEstate(estateName: string) {

    logger.info(`enter loop-estate(${estateName})`)

    const browser = await puppeteer.launch({ headless: true, defaultViewport: undefined });
    const page = await browser.newPage();
  
    let year = new Date().getFullYear();
    let lastYear = 2010
    let estate = estateName;
   

    let filename = filePrefix + new Date().getTime() + ".csv";
    let checkCurrentPageYear = true
    let checkEstateName = true
    while (year >= lastYear) {
        let pageNumber = 1;
        let nextButtonExist: Boolean = true
        let oldPropertyTexts = new Map()
        while (true) {
            await page.setUserAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 11_1_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.96 Safari/537.36");
            await page.goto(`https://www.property.hk/tran.php?dt=&bldg=${estate}&year=${year}&prop=&saleType=3&loc=3&page=${pageNumber}`);
            await page.waitForSelector('#proplist');
            logger.info(`Scrapping - Estate:${estateName} Page:${pageNumber} Year:${year}`);

                const propertyTexts = await page.evaluate(() => {
                const properties = document.querySelectorAll('#proplist > div:nth-child(1) > form > table.table.table-hover.hidden-xs > tbody>tr');
                return Array.from(properties).map((r, i) => {
                    return {

                        registerDate: (r.querySelector(`tr:nth-child(${i + 1}) > td:nth-child(2)`) as HTMLElement)?.innerText,
                        address: (r.querySelector(`tr:nth-child(${i + 1}) > td:nth-child(3)`) as HTMLElement)?.innerText,
                        floor: (r.querySelector(`tr:nth-child(${i + 1}) > td:nth-child(4)`) as HTMLElement)?.innerText,
                        flat: (r.querySelector(`tr:nth-child(${i + 1}) > td:nth-child(5)`) as HTMLElement)?.innerText,
                        areaInFeet: (r.querySelector(`tr:nth-child(${i + 1}) > td:nth-child(6)`) as HTMLElement)?.innerText,
                        dealInMillion: (r.querySelector(` tr:nth-child(${i + 1}) > td:nth-child(7)`) as HTMLElement)?.innerText,
                        pricePerSqrFoot: (r.querySelector(`tr:nth-child(${i + 1}) > td:nth-child(8)`) as HTMLElement)?.innerText,

                    }
                });
            });
            

            let propertyTextsCopy = propertyTexts.filter(value => Object.keys(value).length !== 0)
            logger.info(`Scraped rows: ${propertyTextsCopy.length}`)
           

            checkCurrentPageYear = propertyTextsCopy.some(i => i.registerDate.includes(JSON.stringify(year)))
            
            if (!checkCurrentPageYear) {
                logger.info(`Now scraping ${year} but the content year does not match.`)
                logger.info('End of List')
                break
            }
            checkEstateName = propertyTextsCopy.some(i => i.address.includes(estate))
            
            if (!checkEstateName) {
                logger.info(`Now scraping ${estate} but the estate name in address does not match.`)
                logger.info('End of List')
                break
            }



            let repeatTime: number = 0
            let repeatRate: number = 0

            propertyTextsCopy.forEach(newElement => {
                if (oldPropertyTexts.has(JSON.stringify(newElement))) {
                    repeatTime++
                }

            })

            repeatRate = repeatTime / (oldPropertyTexts.size) * 100
            repeatRate = repeatRate ? repeatRate : 0
            logger.info(`repeatRate(%): ${repeatRate}%`)


            oldPropertyTexts = new Map()
          

            let i = 0
            propertyTextsCopy.forEach(newElem => {
               
                oldPropertyTexts.set(JSON.stringify(newElem), i)
                i++
            })
          


            if (repeatRate < 50) {
                try {
                    if (!fs.existsSync(filename)) {
                        logger.info(`Create a file:${filename} for ${estateName}`)
                        await fs.promises.writeFile(filename, columns.join(',') + "\n", { flag: 'a' })
                    }
                } catch {
                    logger.info('File already exists.')
                }
                propertyTextsCopy.forEach(async (property) => {
                    const row = Object.values(property).join(',');
                    await fs.promises.writeFile(filename, row + "\n", { flag: 'a' });
                });
            }

            nextButtonExist = await page.evaluate(() => {
                const propList: HTMLElement | null = document.querySelector("#proplist")
                const myTest: any = propList?.innerText
                return myTest.includes("下頁")
             
            });

            if (!nextButtonExist) {
                logger.info('No NEXT button.')
            }

            if (!nextButtonExist || repeatRate >= 50) {
                console.log("End of List");
                break;
            }

            pageNumber++;
            await page.waitForTimeout(2000);
        }
        year--

    }


    await browser.close();
    
}






async function readEstate(filePath: string) {
    const estateName = await fs.promises.readFile(filePath, { encoding: 'utf-8' })
    let estateNameNoSpace: String = estateName.replace(/ /g, '')
    estateNameNoSpace = estateNameNoSpace.replace(/^(?=\n)$|^\s*|\s*$|\n\n+/gm, "")
    const arr = estateNameNoSpace.split('\n')
    return arr
}

async function main(filePath: string) {
    logger.log('info', 'begin read estate array...')
    const estateArr = await readEstate(filePath)

    for (let estateName of estateArr) {
        logger.info('enter for-loop...')
      
        await loopEstate(estateName);
       
    }
}


const filePath = './estate-shatin.csv'

main(filePath);

