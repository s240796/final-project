import fs from 'fs';

const estateNameListFolder = './datasets/estate-name-list-raw/estate-name-list-nt.txt';
const targetFolder = './datasets/estate-name-list/estate-name-list-nt.txt';



async function readEstate(filePath: string) {
    const estateNameList = await fs.promises.readFile(filePath, { encoding: 'utf-8' })
  
    return estateNameList
}

async function writeEstate(filePath: string,data:string){
    await fs.promises.writeFile(filePath,data,"utf-8")
}


async function main() {
    const estateNameRawList = await readEstate(estateNameListFolder)
    const temp1 = estateNameRawList.replace(/\)/gm,"),")
    const temp2 = temp1.replace(/\,/gm, "\n")
    const temp3 = temp2.replace(/\([\s\S]*?\)/gm, "")
    const temp4 = temp3.replace(/[\s*\t*]\n[\s*\t*]/gm,"\n")
    const temp5 = temp4.replace(/\n(^[ \t]*\n)/gm,"\n")
   
    await writeEstate(targetFolder,temp5)
    const estateNameNewList = await readEstate(targetFolder)
    console.log(estateNameNewList)

}

main()
