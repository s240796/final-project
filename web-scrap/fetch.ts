import fs from 'fs';
import parse from 'csv-parse';
import util from 'util';
import fetch from 'node-fetch';
import { logger } from './logger';
import path from 'path';


const timeout = util.promisify(setTimeout);


interface PropertyData {

    registerDate: string,
    address: string,
    floor: string,
    flat: string,
    areaInFeet: number,
    dealInMillion: number,
    pricePerSqrFoot: number,
    detail: string,
    others: string
}

const timeBetweenReq = 60; // in ms

async function readCsv(filename: string) {
    logger.info(`Begin to read csv: ${filename}`)
    const parser = fs
        .createReadStream(filename)
        .pipe(
            parse(
                {
                    delimiter: ",",
                    columns: true
                }
            )
        );

    for await (const record of parser) {
        const combined: PropertyData = {

            registerDate: record.registerDate,
            address: record.address,
            floor: record.floor,
            flat: record.flat,
            areaInFeet: parseInt(record.areaInFeet),
            dealInMillion: parseInt(record.dealInMillion),
            pricePerSqrFoot: parseInt(record.pricePerSqrFoot),
            detail: record.detail,
            others: record.others

        };
        await callServer(combined);  
        

        logger.info(`fetched: ${JSON.stringify(combined)}`)
        await timeout(timeBetweenReq);
    }
}

async function callServer(record: PropertyData) {
    return await fetch('http://localhost:8080/transactionRecords', {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(record)
    })
}

const datasetDir = './datasets/shatin/'

fs.readdir(datasetDir,(err,files)=>{
    files.forEach((file)=>{
        const filePath = path.resolve(datasetDir+file)
        
        readCsv(filePath)
    })
})
