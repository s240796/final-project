import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('deal_record',(table)=>{
        table.string('address2').nullable()
        table.string('address7').nullable()
        table.dropColumn('address6')
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('deal_record',(table)=>{
    table.dropColumn('address2')
    table.dropColumn('address7')
    table.string('address6').nullable()

});
}

