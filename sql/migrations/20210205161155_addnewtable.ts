import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable('location',(table)=>{
        table.increments()
        table.string('name').nullable()
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable('location')

}

