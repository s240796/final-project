import * as Knex from "knex";


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable('deal_record',(table)=>{
        table.string('address1').nullable()
        table.string('address6').nullable()
    });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.alterTable('deal_record',(table)=>{
        table.dropColumn('address1')
        table.dropColumn('address6')
    });
}

