1. put your own .env file inside ./sql/, the db_name = daepj
2. cd into folder ./sql/
3. run 'yarn' in folder /sql/ to install missing packages
4. in psql: 'create DATABASE daepj'
5. in vscode/terminal: 'yarn knex migrate:latest'
6. now your database is with the latest table.